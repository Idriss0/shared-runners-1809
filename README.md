# Step 1 : Create a GKE Cluster

Create a GKE cluster with 1 node through GitLab using the [GKE integration](https://www.youtube.com/watch?v=u3jFf3tTtMk). Here we provisioned a 1 node `n2-standard-1` GitLab managed cluster. You can name it anything you like, but in this example we named it `gitlab-windows-1809`. 

>**Note 1** : Please make sure your GKE Cluster version is at a MINIMUM of **1.14.X**. When GitLab provisions the GKE cluster, you may have to upgrade it manually via CLI or the console which has a button to upgrade any cluster to the desired version. Refer to [this documentation](https://cloud.google.com/kubernetes-engine/docs/how-to/upgrading-a-cluster#upgrade_master) for more information. 

>**Note 2** : Within GitLab, you can also deploy Nginx Ingress Controller to front end your web service.

# Step 2 : Provision a Service Account

Create a service account for gcloud interrogation. 

```powershell

# Create a service account
gcloud iam service-accounts create [SA-NAME] --display-name "[SA-DISPLAY-NAME]"

# Download the service account key and store it in a json file
#     [SA-EMAIL] is [SA-NAME]@[PROJECT-ID].iam.gserviceaccount.com
gcloud iam service-accounts list
gcloud iam service-accounts keys create [PATH-TO-KEY.json] --iam-account [SA-EMAIL]

# Add IAM policy to bind the service account to the editor role
gcloud projects add-iam-policy-binding [PROJECT-ID] --member "serviceAccount:[SA-EMAIL]" --role "roles/editor"

# Activate the service account for gcloud
gcloud auth activate-service-account --key-file=[PATH-TO-KEY.json]

```

# Step 3 : Create a Windows Node Pool

Here we'll provision a Windows instance as a node in our GKE cluster. 

```powershell
# Create a node pool with the WINDOWS_SAC image type and the n1-standard-2 machine type
gcloud container node-pools create windows --cluster=[TAREGET-CLUSTER] --image-type=WINDOWS_SAC --machine-type=n1-standard-2 --num-nodes=1
```

>**Note**: You can enter the following command : `gcloud container cluster list` to see the name of the desired GKE cluster. In this example, we use `gitlab-windows-1809`

>**Note:** Check out more technical information on provisioning Windows Nodes on GKE in this [user guide](https://docs.google.com/document/d/1Z3ApFsabqGrKVP_MOBYz5JeJusCkTuoxQQGxmhnwUhw/edit#heading=h.9cnhjclly1kq)

# Step 4 : Kick off CI/CD pipeline to deploy to a Windows node in GKE

Here we'll update the code and kick off a pipeline defined in [`.gitlab-ci.yml`](./.gitlab-ci.yml). Basically the pipeline will build a simple IIS web page hosted in a single docker container. Then the pipeline will use `kubectl` to deploy the service as defined in the `deployment.yml` file. 

To kick off a pipeline, simply commit any changes to this `README.MD` file and push to this project. 


# Notes

To ensure your pods are correctly scheduled onto Windows nodes, you will need to add a node selector to your pod specification within your [`deployment.yml`](./deployment.yml#L17) file.

```yaml
nodeSelector:
  kubernetes.io/os: windows
```

To ensure your CI Runtime environment supports building and executing Windows applications, ensure you have a Windows Runner configured as per Step 4 above and that your [`.gitlab-ci.yml`](./.gitlab-ci.yml#L1) file has right label annotation `windows` identifying which runners can execute the job. Read more [here](https://about.gitlab.com/blog/2020/01/21/windows-shared-runner-beta/index.html).

```yaml
ci_job:
  tags:
  - shared
  - windows
  - windows-1809
 ```
